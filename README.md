# My project README
Simulazione

#Tutorial ed aiuti per ROS+GAZEBO
http://gazebosim.org/tutorials?cat=connect_ros

#Aiuto PX4
https://dev.px4.io/simulation-gazebo.html

#Tutorial modelli SDF
http://sdformat.org/spec?ver=1.6&elem=sensor#sensor_type

#Lanciare un modello in Gazebo
Per lanciare un modello, spostarsi nella cartella di tale modello da terminale e fare gazebo (nome).launch oppure gazebo (nome).world 

#Usare Gazebo+ROS
catkin build from catkin_ws
source ~/catkin_ws/devel/setup.bash

roscore & rosrun gazebo_ros gazebo

#To verify that the proper ROS connections are setup, view the available ROS topics:
rostopic list

You can also verify the Gazebo services exist:
rosservice list

Additionally, you can start Gazebo using roslaunch

roslaunch gazebo_ros empty_world.launch

(Example roslaunch command --> Normally the default values for these arguments are all you need, but just as an example:
	roslaunch gazebo_ros empty_world.launch paused:=true use_sim_time:=false gui:=true throttled:=false headless:=false debug:=true)


Spawn a new model in an open Gazebo world

1) Load gazebo with roslaunch iris_with_standoffs_gazebo iris_with_standoffs.launch
2) In an another terminal, go to catkin_ws/src
3) rosrun gazebo_ros spawn_model -sf -file (path_del_file) -sdf -model (nome_modello)

[rosrun gazebo_ros spawn_model -h per vedere tutte le opzioni]
