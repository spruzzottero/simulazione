#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/alessandro/catkin_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig"
export PWD="/home/alessandro/catkin_ws/build/catkin_tools_prebuild"
export ROSLISP_PACKAGE_DIRECTORIES="/home/alessandro/catkin_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/alessandro/catkin_ws/build/catkin_tools_prebuild:/home/alessandro/catkin_ws/src/baxter_common/baxter_common:/home/alessandro/catkin_ws/src/baxter_common/baxter_core_msgs:/home/alessandro/catkin_ws/src/baxter_common/baxter_description:/home/alessandro/catkin_ws/src/baxter_common/baxter_maintenance_msgs:/home/alessandro/catkin_ws/src/iris_with_standoffs_description:/home/alessandro/catkin_ws/src/iris_with_standoffs_gazebo:/home/alessandro/catkin_ws/src/baxter_common/rethink_ee_description:/opt/ros/kinetic/share"