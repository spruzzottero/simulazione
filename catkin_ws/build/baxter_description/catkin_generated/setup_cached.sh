#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/alessandro/catkin_ws/devel/.private/baxter_description:$CMAKE_PREFIX_PATH"
export PWD="/home/alessandro/catkin_ws/build/baxter_description"
export PYTHONPATH="/home/alessandro/catkin_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/alessandro/catkin_ws/devel/.private/baxter_description/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/alessandro/catkin_ws/src/baxter_common/baxter_description:$ROS_PACKAGE_PATH"