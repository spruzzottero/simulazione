#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/alessandro/catkin_ws/devel/.private/iris_with_standoffs_gazebo:$CMAKE_PREFIX_PATH"
export PWD="/home/alessandro/catkin_ws/build/iris_with_standoffs_gazebo"
export ROSLISP_PACKAGE_DIRECTORIES="/home/alessandro/catkin_ws/devel/.private/iris_with_standoffs_gazebo/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/alessandro/catkin_ws/src/iris_with_standoffs_gazebo:$ROS_PACKAGE_PATH"